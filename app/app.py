from flask import Flask, request, session, g, url_for, abort, render_template,make_response,redirect
import requests
from bs4 import BeautifulSoup as BS
import re
from selenium import webdriver
import selenium
from webdriver_manager.chrome import ChromeDriverManager
from mysql_db import MySQL
app = Flask(__name__)
app.debug = 1
app.config.from_pyfile('config.py')
mysql = MySQL(app)
@app.route('/')
def Main_paper():
	return render_template('index.html',)
@app.route('/input_dictionary')
def input_dictionary():
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select * from wfrn549j_NameInputsEN') 
	input_dictionaryEN = cursor.fetchall()
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select * from wfrn549j_NameInputsRU')
	input_dictionaryRU = cursor.fetchall()
	return render_template('input_dictionary.html', input_dictionaryEN = input_dictionaryEN, input_dictionaryRU = input_dictionaryRU,)
@app.route('/policy_dictionary')
def policy_dictionary():
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select * from sdufvj574_PolicyLinkRU') 
	policy_dictionaryRU = cursor.fetchall()
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select * from sdufvj574_PolicyLinkEN') 
	policy_dictionaryEN = cursor.fetchall()
	return render_template('policy_dictionary.html', policy_dictionaryRU = policy_dictionaryRU, policy_dictionaryEN = policy_dictionaryEN,)
@app.route('/delEN', methods=['POST'])
def del_inputEN():
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('delete from wfrn549j_NameInputsEN where id=%s', (id,))
	mysql.connection.commit()
	return redirect(url_for('input_dictionary'))
@app.route('/delRU', methods=['POST'])
def del_inputRU():
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('delete from wfrn549j_NameInputsRU where id=%s', (id,))
	mysql.connection.commit()
	return redirect(url_for('input_dictionary'))
@app.route('/del-policyRU', methods=['POST'])
def del_policyRU():
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('delete from sdufvj574_PolicyLinkRU where id=%s', (id,))
	mysql.connection.commit()
	return redirect(url_for('policy_dictionary'))
@app.route('/del-policyEN', methods=['POST'])
def del_policyEN():
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('delete from sdufvj574_PolicyLinkEN where id=%s', (id,))
	mysql.connection.commit()
	return redirect(url_for('policy_dictionary'))
@app.route('/updateEN', methods=['POST'])
def update_inputEN():
	lan = 0
	check = 0
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from wfrn549j_NameInputsEN where id=%s', (id,)) 
	input_word = cursor.fetchone()
	return render_template('update_insert.html', input_word = input_word[0], id = id, check = check, lan = lan,)
@app.route('/updateRU', methods=['POST'])
def update_inputRU():
	lan = 1
	check = 0
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from wfrn549j_NameInputsRU where id=%s', (id,)) 
	input_word = cursor.fetchone()
	return render_template('update_insert.html', input_word = input_word[0], id = id, check = check, lan = lan,)
@app.route('/update-policyRU', methods=['POST'])
def update_policyRU():
	lan = 0
	check = 1
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from sdufvj574_PolicyLinkRU where id=%s', (id,)) 
	policy_word = cursor.fetchone()
	return render_template('update_insert.html', policy_word = policy_word[0], id = id, check = check, lan = lan,)
@app.route('/update-policyEN', methods=['POST'])
def update_policyEN():
	lan = 1
	check = 1
	id = request.form.get('id')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from sdufvj574_PolicyLinkEN where id=%s', (id,)) 
	policy_word = cursor.fetchone()
	return render_template('update_insert.html', policy_word = policy_word[0], id = id, check = check, lan = lan,)
@app.route('/insert', methods=['POST'])
def insert_input():
	check = 0
	return render_template('update_insert.html', check = check,)
@app.route('/insert-policy', methods=['POST'])
def insert_policy_input():
	check = 1
	return render_template('update_insert.html', check = check,)
@app.route('/update-wordEN', methods=['POST'])
def update_word_inputEN():
	id = request.form.get('id')
	word = request.form.get('word')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('UPDATE wfrn549j_NameInputsEN SET word=%s WHERE id=%s', (word, id,))
	mysql.connection.commit()
	return redirect(url_for('input_dictionary'))
@app.route('/update-wordRU', methods=['POST'])
def update_word_inputRU():
	id = request.form.get('id')
	word = request.form.get('word')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('UPDATE wfrn549j_NameInputsRU SET word=%s WHERE id=%s', (word, id,))
	mysql.connection.commit()
	return redirect(url_for('input_dictionary'))
@app.route('/update-word-policyRU', methods=['POST'])
def update_word_policyRU():
	id = request.form.get('id')
	word = request.form.get('word')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('UPDATE sdufvj574_PolicyLinkRU SET word=%s WHERE id=%s', (word, id,))
	mysql.connection.commit()
	return redirect(url_for('policy_dictionary'))
@app.route('/update-word-policyEN', methods=['POST'])
def update_word_policyEN():
	id = request.form.get('id')
	word = request.form.get('word')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('UPDATE sdufvj574_PolicyLinkEN SET word=%s WHERE id=%s', (word, id,))
	mysql.connection.commit()
	return redirect(url_for('policy_dictionary'))
@app.route('/insert-word', methods=['POST'])
def insert_word_inputEN():
	lan = request.form.get('lan')
	word = request.form.get('word')
	cursor = mysql.connection.cursor(named_tuple=True)
	if lan == "EN":
		cursor.execute('INSERT INTO wfrn549j_NameInputsEN (id, word) VALUES (null, %s)', (word,))
	elif lan == "RU":
		cursor.execute('INSERT INTO wfrn549j_NameInputsRU (id, word) VALUES (null, %s)', (word,))
	mysql.connection.commit()
	return redirect(url_for('input_dictionary'))
@app.route('/insert-word-policy', methods=['POST'])
def insert_word_policy():
	lan = request.form.get('lan')
	word = request.form.get('word')
	cursor = mysql.connection.cursor(named_tuple=True)
	if lan == "RU":
		cursor.execute('INSERT INTO sdufvj574_PolicyLinkRU (id, word) VALUES (null, %s)', (word,))
	elif lan == "EN":
		cursor.execute('INSERT INTO sdufvj574_PolicyLinkEN (id, word) VALUES (null, %s)', (word,))
	mysql.connection.commit()
	return redirect(url_for('policy_dictionary'))
@app.route('/parser', methods=['POST'])
def parser():
	registration_dictionary = ["Регистрация",
							   "регистрация",
							   "Зарегистрироваться",
							   "зарегистрироваться"]
	all_inputs = []
	all_links = []
	pd_links = []
	url2 = ""
	input_name_dictionary_list = []
	policy_input_dictionary_list = []
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from wfrn549j_NameInputsEN') 
	input_name_dictionary_list = cursor.fetchall()
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from wfrn549j_NameInputsRU') 
	input_name_dictionary_list += cursor.fetchall()
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from sdufvj574_PolicyLinkRU') 
	policy_input_dictionary_list = cursor.fetchall()
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select word from sdufvj574_PolicyLinkEN') 
	policy_input_dictionary_list += cursor.fetchall()
	url = request.form.get('url')
	if url == "":
		url_err = 'вы не ввели url'
		return render_template('index.html', url_err = url_err,)
	options = webdriver.ChromeOptions()
	options.add_argument('headless')
	driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
	driver.set_window_size(1120, 550)
	driver.get(url)
	for i in range(len(registration_dictionary)):
		try:
			driver.find_element_by_partial_link_text(registration_dictionary[i]).click()
			url2 = driver.current_url
		except selenium.common.exceptions.NoSuchElementException:
			if url2 != "":
				continue
	if url2 == "":
		url2 = url
	try:
		page = requests.get(url2)
	except requests.exceptions.MissingSchema:
		url_err = 'вы не ввели url'
		return render_template('index.html', url_err = url_err, url2 = url2,)
	page.encoding = 'utf-8'
	soup = BS(page.text, "html.parser")
	for i in input_name_dictionary_list:
		all_inputs += soup.findAll('input', attrs={"name" : "{}".format(i.word)})
	all_links = soup.findAll('a')
	for j in policy_input_dictionary_list:
		for i in range(len(all_links)):
			if all_links[i].text == j.word:
				pd_links.append(all_links[i])
	status = 4
	if all_inputs == []:
		status = 0
	elif all_inputs != []:
		if pd_links == []:
			status = 1
		elif pd_links != []:
			status = 2
	DomainName = re.search('https?://([A-Za-z_0-9.-]+).*', url)
	return render_template('index.html', status = status, pd_links = pd_links, all_inputs = all_inputs, url2 = url2, DomainName = DomainName,)
if __name__ == "__main__":
	app.run()