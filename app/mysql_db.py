import mysql.connector
from flask import current_app

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack

class MySQL():
    def __init__(self, app=None, **connect_args):
        self.connector = mysql.connector
        self.connect_args = connect_args
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

    def connect(self):
        self.connect_args['host'] = self.app.config.get('MYSQL_DATABASE_HOST', 'localhost')
        self.connect_args['port'] = self.app.config.get('MYSQL_DATABASE_PORT', 3306)
        self.connect_args['user'] = self.app.config.get('MYSQL_DATABASE_USER')
        self.connect_args['password'] = self.app.config.get('MYSQL_DATABASE_PASSWORD')
        self.connect_args['db'] = self.app.config.get('MYSQL_DATABASE_DB')
        self.connect_args['use_pure'] = self.app.config.get('MYSQL_DATABASE_USE_PURE', True)
        self.connect_args['raise_on_warnings'] = self.app.config.get('MYSQL_DATABASE_RAISE_ON_WARNINGS', False)
        return mysql.connector.connect(**self.connect_args)

    def teardown(self, exception):
        ctx = stack.top
        if hasattr(ctx, 'mysql_db'):
            ctx.mysql_db.close()

    @property
    def connection(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'mysql_db'):
                ctx.mysql_db = self.connect()
            return ctx.mysql_db